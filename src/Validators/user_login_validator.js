const { body } = require('express-validator/check');

module.exports.loginValidator = [
  body('email', 'email is required').exists(),
  body('email','email not conformed').isEmail(),
  body('md5_password', 'password is required').exists()
];
