

var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var dbManager = require('./databaseManager.js')
const validate_user = require('./Validators/user_login_validator.js').loginValidator
const helloWorld = require('./MiddleWare/helloWorld.js').helloWorld
const user_val_checker = require('./MiddleWare/user_validator_check.js').handleValidationResult

//
const url = process.env.USER_DB_URL || '192.168.99.100'  //"db_service" //'192.168.99.100'
const port = process.env.USER_DB_PORT || 5984
const protocol = 'http'
const userDBName = 'user_database'

//Initialize the server instance to port 8080 & Start app listening
// for parsing application/json
app.use(bodyParser.json());
var server = app.listen(8080, function () {
   var host = server.address().address
   var port = server.address().port

   console.log("USER endpoint listening at http://%s:%s", host, port)
   console.log('\n\n\n DB url: '+ process.env.USER_DB_URL)
})

//==================================
//   HEALTH TEST
//==================================
app.get('/healthcheck',helloWorld )

//==================================
//   AUTHENTICATE - LOGIN
//==================================
//according to express-validator lib -> path, validator, check to return errors, req/res function if all is correct
app.post("/user/login",validate_user,user_val_checker, (req,res) => {


  //0. setup content type of json
  res.setHeader('Content-Type', 'application/json');


  //1. connect into DB OR CREATE IT
  dbManager.createDB(protocol, url, port, userDBName, (err) => {

      //add error status code according API documentation
      res.status(400);
      res.send(JSON.stringify({error: err.message} ))
      console.log("Error: "+err.message);

  }, (success, db) => {

      console.log("Success created or found db: "+ success);

      //2. retrive a particular user
      dbManager.login(req.body.email, req.body.md5_password, db, userDBName)
      .then(
        function resolve(body){

          res.status(200).json(body.doc)

      },function reject(rej){

        rej.message != "That user not exists" ?
          res.status(400).json({reason: rej.message})
          : res.status(423).json({reason: "user not exists"})

      })
  })//end success create DB






})//end app.get("/user/login"

//==================================
//      GET ALL THE USERS /user GET
//==================================
app.get('/user', (req,resp) => {

  //0. setup content type of json
  resp.setHeader('Content-Type', 'application/json');

  //1. connect into DB OR CREATE IT
  dbManager.createDB(protocol, url, port, userDBName, (err) => {

      //add error status code according API documentation
      resp.status(400);
      resp.send(JSON.stringify({error: err.message} ))
      console.log("Error: "+err.message);

  }, (success, db) => {
      console.log("Success created or found db: "+ success);

      //2. get list of users
      dbManager.get_all_users(db, userDBName, (err) =>{
        resp.send(err)
      }, (success) => {

        var arrayUser = []

        //3. for every row extract the real document object and add it into array
        success.rows.forEach(function(doc) {

          //4. ADD USER TO ARRAY
          arrayUser.push(doc.doc)

        });

        //5.A Setup status code
        resp.status(200)

        //5.B return the complete array of the users
        resp.send(arrayUser)

      })//end get all users callback
  })//end create DB callback
})//end all get list of users express call

//==================================
//      DELETE THE USER /user DELETE
//==================================
app.delete('/user',(req,res) =>{

  //0. setup content type of json
  res.setHeader('Content-Type', 'application/json');

  //1. connect into DB OR CREATE IT
  dbManager.createDB(protocol, url, port, userDBName, (err) => {

      //add error status code according API documentation
      res.status(400);
      res.send(JSON.stringify({error: err.message} ))
      console.log("Error: "+err.message);

  }, (success, db) => {
      console.log("Success created or found db: "+ success);

      //2. delete all the users
      dbManager.delete_all_users(db, userDBName, (error) =>{

        res.status(400)
        res.send(JSON.stringify({ result: 'error'}))

      }, (success) =>{

        res.status(200)
        res.send(JSON.stringify({ result: 'success'}))
      })
  })//end create DB callback


})//end DELETE THE USER /user DELETE

//==================================
//      ADD NEW USER /user POST
//==================================##
app.post('/user', (req, res) => {

  //0. setup content type of json
  res.setHeader('Content-Type', 'application/json');

  //1.parse parameter othervise send appropriated status code
  console.log("Request body"+  req.body)

  if(req.body.name == null || req.body.surname == null || req.body.email == null
    || req.body.password_md5 == null){

      //2.A No appropriated para eters, sed correctly status code according documentation
      res.status(422)
      res.send(JSON.stringify({ error: "wrong parameters"}))
    }else{
      //here the parameters are ok
      //3. create DB if not exist with the according DB name
      dbManager.createDB(protocol, url, port, userDBName, (err) => {

          console.log("Error: "+err.message);
          res.status(400)
          res.send(JSON.stringify({error: err.message} ))
          return

      }, (success, db) => {

          console.log("Success: "+ success);

          //1. add USER (without ID == auto generated id)
          db.use(userDBName).insert({
            name: req.body.name ,
            surname: req.body.surname,
            email : req.body.email,
            password_md5: req.body.password_md5,
            date: new Date().getTime() },  function(err, body, header) {

          if (err) {
            console.log('[inserting new user] ', err.message);
            res.status(400)

            res.send(JSON.stringify(err.message))

          }else{
            console.log('you have inserted the new user.')
            res.status(200)
            res.send(JSON.stringify(body))
          }

        });
      })
    }//end else check parameter
})//end add user fucntion

//==================================
//  GET INFO PARTICULAR USER GET + PATH PARM
//==================================
app.get('/user/:userID', (req,res) =>{

  //0. setup content type of json
  res.setHeader('Content-Type', 'application/json');

  //1.parse parameter othervise send appropriated status code
  console.log("/user/{userdID}: "+  req.params.userID)

  //2. connect into DB

  dbManager.createDB(protocol, url, port, userDBName, (err) => {

      //add error status code according API documentation
      res.status(400);
      res.send(JSON.stringify({error: err.message} ))
      console.log("Error: "+err.message);

  }, (success, db) => {

      //3.get info about user
      dbManager.get_user_given_id (db,userDBName,req.params.userID,(err) =>{

        res.status(400);
        res.send(JSON.stringify({error: err.message} ))
        console.log("Error: "+err.message);

      },(succ) =>{

        console.log("Success");
        if(succ != null){
          res.status(200);
          res.send(succ.doc)
        }else{
          res.status(404);
          res.send(JSON.stringify({reason: "no user with that ID present"} ))

        }


      })



  })//end createDB
})//end GET INFO PARTICULAR USER GET + PATH PARM

//==================================
//  DELETE INFO PARTICULAR USER GET + PATH PARM
//==================================
app.delete('/user/:userID', (req,res) =>{

  //0. setup content type of json
  res.setHeader('Content-Type', 'application/json');

  //1.parse parameter othervise send appropriated status code
  console.log("/user/{userdID}: "+  req.params.userID)

  //2. connect into DB

  dbManager.createDB(protocol, url, port, userDBName, (err) => {

      //add error status code according API documentation
      res.status(400);
      res.send(JSON.stringify({error: err.message} ))
      console.log("Error: "+err.message);

  }, (success, db) => {

      //3.delete the user
      dbManager.delete_user_given_id(db, userDBName, req.params.userID,
        (err) =>{

          res.status(400);
          res.send(JSON.stringify({error: err.message} ))
          console.log("Error: "+err.message);

      }, (success)=>{

        res.status(200);
        res.send(JSON.stringify({result: "user deleted"} ))


      }, (noExt) =>{

        res.status(404);
        res.send(JSON.stringify({reason: "no user with that ID present"} ))

      })//end delete_user_given_id
  })//end createDB
})//end GET INFO PARTICULAR USER GET + PATH PARM
