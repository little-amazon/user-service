



// ------------------------
//  Database manager utility
// ------------------------
module.exports.login =  async function(email, password, nanoDB, dbName) {


    return new Promise(function(resolve, reject) {


      nanoDB.use(dbName).list({include_docs:true}, function(err, body) {
        if (err){
          reject(err)
        }else{

          console.log("module.exports.login  try to find user with:")
          console.log("email: "+email)
          console.log("password: "+password)

          var found = null
          body.rows.forEach(function(rw){

            console.log(rw)
            if(rw.doc.email == email && rw.doc.password_md5 == password){
                found = rw
              }
          })//end for each

          //console.log("Founded or not: "+found)

          if(found == null){
            reject(new Error("That user not exists"))
          }else{
            resolve(found)
          }

          //found != null ? resolve(found) : user_notExists()

        }//end else
      })//end list
    });//end promise function
}//module.exports.login


module.exports.createDB = function createDB(protocol, url, port, dbName, onError, onSuccess ){

  //create a nano db instance for given URL
  var nano = require('nano')(protocol+'://'+url+':'+port);

      // create a new database
      nano.db.create(dbName, (err, body, header) =>  {

        if(err){

          //maybe db already exits
          if( err.message == 'The database could not be created, the file already exists.'){
            onSuccess(body, nano)
          }else{
            onError(err)
          }
        }else{

          //set to use that BD
          nano.db.use(dbName)

          onSuccess(body, nano)
        }

      });//end create callback and create function call

}//end createDB function

// ------------------------
module.exports.get_all_users = (nanoDB, userDBName, onError, onSuccess) => {

  nanoDB.use(userDBName).list({include_docs:true}, function(err, body) {
    if (err){
      onError()
    }else{
      onSuccess(body)
    }
  })
}//end module.exports.findUSER

// ------------------------
module.exports.get_user_given_id = (nanoDB, userDBName, userID, onError, on_Success) => {

  console.log("Preparing to get info about a specific user")
  try{
    nanoDB.use(userDBName).list({include_docs:true}, function(err, body) {
      if (err){
        onError()
      }else{


        console.log(body)
        var found = null
        body.rows.forEach(function( rw){

          console.log(rw)
          if(rw.id == userID){
              found = rw
            }
        })//end for each

        console.log("Founded or not: "+found)
        on_Success(found)

      }//end else
    })//end get list
  } catch(err) {
    console.log("Error occurred")
    onError(er)
  }


}//end module.exports.get_user_given_id

// ------------------------
module.exports.delete_all_users = (nanoDB, userDBName, onError, onSuccess) => {

  nanoDB.use(userDBName).list({include_docs:true}, function(err, body) {
    if (err){
      console.log("ERR")
      onError()
    }else{

        //Setup documents
        const documents = []

        body.rows.forEach(function(doc) {

          //4. ADD USER TO ARRAY
          var toDelete = doc.doc

          //add delete keyword
          toDelete._deleted = true

          documents.push(toDelete)

        });


        nanoDB.use(userDBName).bulk({docs:documents}).then((body) => {
          console.log(body);

          onSuccess(body)
        });

    }
  })
}//end module.exports.findUSER

// ------------------------
module.exports.delete_user_given_id = (nanoDB, userDBName, userID, onError, on_Success, onNotExist) => {

  console.log("Preparing to get info about a specific user")
  try{
    nanoDB.use(userDBName).list({include_docs:true}, function(err, body) {
      if (err){
        onError()
      }else{

        console.log(body)
        var found = null
        body.rows.forEach(function( rw){

          console.log(rw)
          if(rw.id == userID){
              found = rw
            }
        })//end for each

        console.log("Founded or not: "+found)

        //check if the item with that id exists
        if(found == null){

          onNotExist()
        }else{

            //add delete keyword to the document
            found.doc._deleted = true

            //try to remove the object
            nanoDB.use(userDBName).bulk({docs: [found.doc]}).then((body) => {
              console.log(body);

              on_Success(body)
            });//end bulk operation
        }//end else item found
      }//end else no error
    })//end get list
  } catch(err) {
    console.log("Error occurred")
    onError(er)
  }//end catch


}//end module.exports.get_user_given_id
