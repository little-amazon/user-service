'use strict';

const { validationResult } = require('express-validator/check');

function handleValidationResult (req, res, next) {
  let errors = validationResult(req);

  if (errors.isEmpty()) {
    return next();
  }

  let firstError = errors.array()[0];

  return res.status(422).json({ code: 'ValidationError', message: firstError.msg });
}

module.exports = {
  handleValidationResult: handleValidationResult
};
