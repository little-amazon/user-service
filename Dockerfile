FROM node:8

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./


#INSTALL PACKAGES
RUN npm install
# If you are building your code for production
# RUN npm install --only=production

#SETUP THE ENV VARIABLES
ENV DATABASE_URL = localhost
ENV DATABASE_PORT = 5984

# Bundle app source
COPY . .

EXPOSE 8080




CMD [ "npm", "start" ]
