const chai = require("chai");
const expect = chai.expect;
const sinon = require("sinon");
// import our getIndexPage function
const helloWorldPageHandler = require('../src//MiddleWare/helloWorld.js').helloWorld

describe("Health Test", function() {


  it("checks the Healthy of the system", function() {


    let req = {}
    // Have `res` have a send key with a function value coz we use `res.send()` in our func
    let res = {
      send: sinon.spy(),
      end: function(){},
      status: function(s) {this.statusCode = s; return this;}
    }

      //TEST
    helloWorldPageHandler(req,res)

    expect(res.send.calledOnce).to.be.true;
   // expect to get argument `bla` on first call
   expect(res.send.firstCall.args[0]).to.equal("Healthy!");


  });
});
