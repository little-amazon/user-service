# user-service

This repo contains the user sevice module. Into DB_Associated folder is contained
the DB associated service.

##description
A docker file is created to run nodejs
The image has all the file inside and expose PORT 8080
.dockerignore is included to ignore all the dependencies
Dependencies will be installed by the RUN command

The environmental variables are set into Dockefile.

##How to build image
Simply
> docker build -t littleamazon/user-service-node_img .

##How to run image
Simply

Notice you have to be connected to the container db url
> docker run -d --name user_container -p 8080:8080 --link couch_db_container:db_service littleamazon/user-service-node_img
