# How to use this img

Create a couch db img from Dockerfile
> docker build -t mycouch_img .

Create a cointainer and expose the port 5984
> docker run -d --name couch_db_container -p 5984:5984  mycouch_img

##UI

To easily manage the CouchDB content you can use the UI
>http://localhost:5984/_utils/

## VOLUMES

This Dockerfile create two volumes to perseve storage and configurations.

> VOLUME /var/log /opt/couchdb/data

> VOLUME /var/log /opt/couchdb/etc
